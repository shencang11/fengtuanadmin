import logo from "./logo.svg";
import "./App.css";
import { Button, Pagination } from "antd";
import "./App.less";
function App() {
  const itemRender = (_, type, originalElement) => {
    if (type === "prev") {
      return <a href="/asd">上一步</a>;
    }

    if (type === "next") {
      return <a href="/sds">下一步</a>;
    }

    return originalElement;
  };
  return (
    <div className="App">
      <Button type="primary">你好吗</Button>
      <Button>要你寡</Button>
      <Button type="dashed">实际代付是</Button>
      <br />
      <Button type="text">打钩的</Button>
      <Button type="link">司法鉴定看</Button>
      <Pagination total={500} itemRender={itemRender} />
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
