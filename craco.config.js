/* craco.config.js */
const CracoLessPlugin = require("craco-less");
module.exports = {
  // ...
  babel: {
    plugins: [["import", { libraryName: "antd", style: "css" }]],
  },
  plugins: [
    {
      plugin: CracoLessPlugin,
    },
  ],
};
